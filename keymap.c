#include QMK_KEYBOARD_H

#include "chimera_ergo.h"
#include "action_layer.h"

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
enum chimera_ergo_layers
{
	_COLEMAK,
	_QWERTY,
	_LOWER,
	_RAISE,
	_FKEYS,
};

#define LONGPRESS_DELAY 150
//#define LAYER_TOGGLE_DELAY 300

enum {
	TD_OPEN_TERMINAL,
	TD_ALT_SHIFT,
};

// Fillers to make layering more clear
#define _______ KC_TRNS
#define XXXXXXX KC_NO
#define KC_ KC_TRNS
#define KC_TERM LCTL(LALT(KC_V))
#define KC_Lgui TD(TD_OPEN_TERMINAL)
#define KC_Lsft TD(TD_ALT_SHIFT)
#define KC_OPEN_TERMINAL LGUI(KC_ENT)
#define KC_SINS LSFT(KC_INS)
#define KC_LOWR LT(_LOWER, KC_SPC)
#define KC_RASE LT(_RAISE, KC_ENT)
#define KC_AE 0x64
#define KC_OE LSFT(0x64)
#define KC_FKEY MO(_FKEYS)
#define KC_QWER TG(_QWERTY)

qk_tap_dance_action_t tap_dance_actions[] = {
	[TD_OPEN_TERMINAL] = ACTION_TAP_DANCE_DOUBLE(KC_LGUI, KC_OPEN_TERMINAL),
	[TD_ALT_SHIFT] = ACTION_TAP_DANCE_DOUBLE(KC_LSFT, KC_LALT),
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_COLEMAK] = LAYOUT(
     KC_ESC , KC_1  , KC_2  , KC_3  , KC_4  , KC_5   ,       KC_6   , KC_7  , KC_8  , KC_9  , KC_0  ,KC_EQL ,
     KC_TAB , KC_Q  , KC_W  , KC_F  , KC_P  , KC_G   ,       KC_J   , KC_L  , KC_U  , KC_Y  ,KC_SCLN,KC_TERM,
     KC_BSPC, KC_A  , KC_R  , KC_S  , KC_T  , KC_D   ,       KC_H   , KC_N  , KC_E  , KC_I  , KC_O  ,KC_QUOT,
     KC_Lsft, KC_Z  , KC_X  , KC_C  , KC_V  , KC_B   ,       KC_K   , KC_M  ,KC_COMM,KC_DOT ,KC_SLSH,KC_QWER,
                                      KC_LCTL,KC_LOWR,       KC_RASE,KC_Lgui
  ),

  [_QWERTY] = LAYOUT(
     KC_ESC , KC_1  , KC_2  , KC_3  , KC_4  , KC_5   ,       KC_6   , KC_7  , KC_8  , KC_9  , KC_0  ,KC_EQL ,
     KC_TAB , KC_Q  , KC_W  , KC_E  , KC_R  , KC_T   ,       KC_Y   , KC_U  , KC_I  , KC_O  , KC_P  ,KC_TERM,
     KC_BSLS, KC_A  , KC_S  , KC_D  , KC_F  , KC_G   ,       KC_H   , KC_J  , KC_K  , KC_L  ,KC_SCLN,KC_QUOT,
     KC_Lsft, KC_Z  , KC_X  , KC_C  , KC_V  , KC_B   ,       KC_N   , KC_M  ,KC_COMM,KC_DOT ,KC_SLSH,KC_QWER,
                                      KC_LCTL,KC_LOWR,       KC_RASE,KC_Lgui
  ),

  [_LOWER] = LAYOUT(
     _______,KC_MUTE,KC_VOLD,KC_VOLU,_______,_______,      _______,_______,_______,KC_LPRN,KC_RPRN,_______,
     _______,KC_CAPS,KC_PGUP, KC_UP ,KC_PGDN, KC_ESC,      _______,KC_MINS,KC_UNDS,KC_LBRC,KC_RBRC,KC_SINS,
      KC_DEL,KC_HOME,KC_LEFT,KC_DOWN,KC_RGHT, KC_END,      _______,_______,_______,_______,KC_PIPE,_______,
     _______,_______,_______,_______,_______,_______,      _______,_______,_______,KC_BSLS,_______,_______,
                                     _______,_______,      _______,_______
  ),

  [_RAISE] = LAYOUT(
      KC_GRV,_______,_______,_______,_______,_______,      _______,_______,_______,_______,_______,_______,
     _______,_______,KC_WH_U,KC_MS_U,KC_WH_D,_______,      KC_BTN3,KC_BTN1,KC_BTN2,_______,_______,_______,
     _______, KC_AE ,KC_MS_L,KC_MS_D,KC_MS_R,_______,      _______,_______,_______,_______, KC_OE ,_______,
     _______,_______,_______,_______,_______,_______,      _______,_______,_______,_______,_______,_______,
                                     _______,_______,      _______,KC_ACL0
  ),

  [_FKEYS] = LAYOUT(
     KC_F1   , KC_F2  , KC_F3  , KC_F4  , KC_F5  , KC_F6  ,       KC_F7  , KC_F8  , KC_F9  ,KC_F10  ,KC_F11  ,KC_F12 ,
     _______,_______,_______,_______,_______,_______,      _______,_______,_______,_______,_______,_______,
     _______,_______,_______,_______,_______,_______,      _______,_______,_______,_______,_______,_______,
     _______,_______,_______,_______,_______,_______,      _______,_______,_______,_______,_______,_______,
                                     _______,_______,      _______,_______
  ),

};


const uint16_t PROGMEM fn_actions[] = {

};

void matrix_scan_user(void) {
    uint8_t layer = biton32(layer_state);

    switch (layer) {
    	case _COLEMAK:
	    set_led_off;
    	    break;
        case _LOWER:
            set_led_blue;
            break;
        case _RAISE:
            set_led_red;
            break;
        case _FKEYS:
            set_led_magenta;
            break;
       default:
            set_led_off;
            break;
    }
};
